﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagementTestApp.Model
{
    public class UserProfile
    {
        [BsonId]
        [BsonElement("id")]
        public string UserId { get; set; }

        [BsonElement("email")]
        [Required]
        [EmailAddress]
        public string Email { get; set; }


        [BsonElement("firstname")]
        [Required]        
        public string FirstName { get; set; }

        [BsonElement("lastname")]
        [Required]
        public string LastName { get; set; }

        [BsonElement("contactnumber")]
        [Required]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Contact Number.")]
        public string ContactNo { get; set; }


        [BsonElement("createddate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("modifieddate")]
        public DateTime ModifiedDate { get; set; }
        [BsonElement("isactive")]
        public bool IsActive { get; set; }
    }
}
