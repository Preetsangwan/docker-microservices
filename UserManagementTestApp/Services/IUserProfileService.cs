﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagementTestApp.Model;

namespace UserManagementTestApp.Services
{
    public interface IUserProfileService
    {

        Task<bool> AddUser(UserProfile user);
        Task<ICollection<UserProfile>> GetAll();
        Task<UserProfile> GetUserById(string Id);
        Task<bool> UpdateUser(UserModel user);
        Task<bool> UpdateUserStatus(string Id, bool status);
        Task<bool> DeleteUser(string Id);
    }
}
