﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagementTestApp.DataAccess;
using UserManagementTestApp.Model;

namespace UserManagementTestApp.Services
{
    public class UserProfileService : IUserProfileService
    {

        MongoDbContext _db;
        public UserProfileService(MongoDbContext db)
        {
            _db = db;
        }
        public async Task<bool> AddUser(UserProfile usermodel)
        {
            try
            {
                var user = new UserProfile()
                {
                    UserId = Guid.NewGuid().ToString(),
                    Email = usermodel.Email,
                    FirstName = usermodel.FirstName,
                    LastName = usermodel.LastName,
                    ContactNo = usermodel.ContactNo,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsActive = true
                };
                await _db.UserProfile.InsertOneAsync(user);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// DeleteUser
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteUser(string Id)
        {
            try
            {
                await _db.UserProfile.DeleteOneAsync(x => x.UserId == Id);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }


        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<UserProfile>> GetAll()
        {
            try
            {
                return await _db.UserProfile.Find(_ => true).ToListAsync();
            }
            catch (Exception)
            {
                return new List<UserProfile>();
            }

        }

        /// <summary>
        /// GetUserById
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<UserProfile> GetUserById(string Id)
        {
            try
            {
                return await _db.UserProfile.Find(x => x.UserId == Id).FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                return new UserProfile();
            }

        }

        /// <summary>
        /// UpdateUser
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUser(UserModel user)
        {
            try
            {
                var userObject = _db.UserProfile.Find(x => x.UserId == user.UserId).FirstOrDefault();
                if (userObject != null)
                {
                    var updates = new List<UpdateDefinition<UserProfile>>();

                    updates.Add(Builders<UserProfile>.Update.Set(x => x.ModifiedDate, DateTime.Now));
                    updates.Add(Builders<UserProfile>.Update.Set(x => x.Email, user.Email));
                    updates.Add(Builders<UserProfile>.Update.Set(x => x.FirstName, user.FirstName));
                    updates.Add(Builders<UserProfile>.Update.Set(x => x.LastName, user.LastName));
                    updates.Add(Builders<UserProfile>.Update.Set(x => x.ContactNo, user.ContactNo));
                    await _db.UserProfile.FindOneAndUpdateAsync(Builders<UserProfile>.Filter.Eq(x => x.UserId, user.UserId),
                        Builders<UserProfile>.Update.Combine(updates),
                        new FindOneAndUpdateOptions<UserProfile, UserProfile> { ReturnDocument = ReturnDocument.After });

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// UpdateUserStatus
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUserStatus(string Id, bool status)
        {
            try
            {
                var userObject = _db.UserProfile.Find(x => x.UserId == Id).FirstOrDefault();
                if (userObject != null)
                {
                    var updates = new List<UpdateDefinition<UserProfile>>();
                    updates.Add(Builders<UserProfile>.Update.Set(x => x.IsActive, status));
                    await _db.UserProfile.FindOneAndUpdateAsync(Builders<UserProfile>.Filter.Eq(x => x.UserId, Id),
                        Builders<UserProfile>.Update.Combine(updates),
                        new FindOneAndUpdateOptions<UserProfile, UserProfile> { ReturnDocument = ReturnDocument.After });

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
