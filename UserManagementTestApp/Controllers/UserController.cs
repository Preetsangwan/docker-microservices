﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserManagementTestApp.Model;
using UserManagementTestApp.Services;

namespace UserManagementTestApp.Controllers
{

    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserProfileService _user;
        public UserController(IUserProfileService user)
        {
            _user = user;
        }

        /// <summary>
        /// Create New User
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("adduser")]
        public async Task<IActionResult> AddUser([FromBody]UserProfile userInfo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _user.AddUser(userInfo);
                    if (response)
                    {
                        return Ok();
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                return BadRequest(ModelState);

            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.InnerException);
            }
        }

        /// <summary>
        /// Get All User
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var response = await _user.GetAll();
                if (response.Any())
                {
                    return Ok(response);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get user by userid
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyid/{userId}")]
        public async Task<IActionResult> GetById([FromRoute]string userId)
        {
            try
            {
                var response = await _user.GetUserById(userId);
                if (response != null)
                {
                    return Ok(response);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Update User Profile
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateuser")]
        public async Task<IActionResult> UpdateUser([FromBody]UserModel userInfo)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    var response = await _user.UpdateUser(userInfo);
                    if (response)
                    {
                        return Ok(response);
                    }
                    else
                    {
                        return StatusCode(405, "Incorrect input");
                    }
                }
                return StatusCode(405, "Incorrect input");

            }
            catch (Exception ex)
            {

                throw ex.InnerException;
            }
        }

        /// <summary>
        /// Hard delete user by userid
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteuser/{userId}")]
        public async Task<IActionResult> DeleteUser([FromRoute]string userId)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    var response = await _user.DeleteUser(userId);
                    return Ok();
                }
                else
                {
                    return BadRequest("UserId is Required");
                }

            }
            catch (Exception ex)
            {

                throw ex.InnerException;
            }
        }

        /// <summary>
        /// Update User Status Active or Inactive
        /// </summary>
        /// <param name="usermodel"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route("updatestatus/{userId}/{isActive}")]
        public async Task<IActionResult> UpdateUserStatus([FromRoute]string userId, [FromRoute] bool isActive)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    var response = await _user.UpdateUserStatus(userId, isActive);
                    return Ok();
                }
                else
                {
                    return BadRequest("UserId is Required");

                }


            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }
    }
}