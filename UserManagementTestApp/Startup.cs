﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UserManagementTestApp.DataAccess;
using UserManagementTestApp.Services;

namespace UserManagementTestApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {                        
            string mongo_host = Environment.GetEnvironmentVariable("MONGODB_CONNECTION") ?? "localhost";
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("MONGODB_DATABASE"))) Environment.SetEnvironmentVariable("MONGODB_DATABASE", "UserManagementTestApp");
            services.AddTransient<MongoDbContext>();

            services.AddSingleton<IUserProfileService, UserProfileService>();
            services.AddMvc();
            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
